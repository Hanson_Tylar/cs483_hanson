/*
 Tylar Hanson
 Documentation: None
 */

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "pex0.h"

    // D
int funct1(int array[]){
    return array[0]*2;
}
    // A
int main(int argc, char **argv) {
    // B
    printf("The first argument is: %s.\n\n", argv[1]);

    // C
    for(int i=0; i<21; i++){
        if(i%3==0){
            printf("%d\t",i);
        }
    }
    printf("\n\n");

    // E
    int myArray[1] = {5};
    int ret_val = funct1(myArray);
    printf("Result of 2*myArray[0] is: %d.\n\n", ret_val);

    // G, H, I, & J
    Node node1, node2;

    strncpy(node1.name, "Node 1", 7);
    node1.next = &node2;

    strncpy(node2.name, "Node 2", 7);
    node2.next = NULL;


    printf("Name of node1 is '%s'.\n",node1.name);
    printf("Name of node2 is '%s'.\n",node1.next->name);

    // K
    NodePtr node3;
    node3 = malloc(sizeof(struct node));
    strncpy(node3->name,"Node 3", 7);
    node3->next = NULL;
    printf("Name of node3 is '%s'.\n", node3->name);
    return 0;
}