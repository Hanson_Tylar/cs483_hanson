#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "list.h"

node* list_insert_tail(node* list, char* new_data) {
    node *temp;
    if(list==NULL){
        temp = malloc(sizeof(struct s_node));
        temp->data = malloc(strlen(new_data)+1);
        strncpy(temp->data, new_data, strlen(new_data)+1);
        temp->next = NULL;
        return temp;
    }
    else{
        temp->next = list_insert_tail(list->next, new_data);
        return temp;
    }
    return NULL;
}

node* list_insert_head(node* list, char* new_data) {
    node *temp;
    if(list==NULL){
        temp = malloc(sizeof(struct s_node));
        temp->data = malloc(strlen(new_data)+1);
        strncpy(temp->data, new_data, strlen(new_data)+1);
        temp->next = NULL;
        return temp;
    }
    else{
        temp = malloc(sizeof(struct s_node));
        temp->data = malloc(strlen(new_data)+1);
        strncpy(temp->data, new_data, strlen(new_data)+1);
        temp->next = list;
        return temp;
    }
    return NULL;
}

node* list_insertn(node* list, char* new_data, int n) {
    node *temp;
    if(list==NULL){
        temp = malloc(sizeof(struct s_node));
        temp->data = malloc(strlen(new_data)+1);
        strncpy(temp->data, new_data, strlen(new_data)+1);
//        temp->data = new_data;
        temp->data = new_data;
        return temp;
    }
    else if(n==1){
        temp = malloc(sizeof(struct s_node));
        temp->next = list;
        temp->data = new_data;
        return temp;
    }
    else{
        list->next = list_insertn(list->next, new_data, n-1);
        return list;
    }
    return NULL;
}

node* list_remove(node* list, char* data) {
    if(list==NULL){
        return NULL;
    }
    else if(strcmp(list->data, data) == 0){
        node *temp = list->next;
        free(list);
        return temp;
    }
    else{
        list->next = list_remove(list->next, data);
        return list;
    }
    return NULL;
}

node* list_removen(node* list, int n) {
    if(list==NULL){
        return NULL;
    }
    else if(n==1){
        node *temp = list->next;
        free(list);
        return temp;
    }
    else{
        list->next = list_removen(list->next, n-1);
        return list;
    }
    return NULL;
}

void list_print(node* list, int i) {
    if(list==NULL | i > 10){
        return;
    }
    else{
        printf("%d.) %s\n", i, list->data);
        list_print( list->next, i+1);
    }

}

void list_printn(node* list, int n) {
    if(list==NULL){
        return;
    }
    else if(n==1){
        printf("%s", list->data);
        return;
    }
    else {
        return list_printn(list->next, n-1);
    }
}

char* list_get(node* list, int n) {
    if(list==NULL){
        return NULL;
    }
    else if(n==1){
        return list->data;
    }
    else {
        return list_get(list->next, n-1);
    }
}

void list_destroy(node* list) {
    if(list==NULL){
        return;
    }
    else{
        list_destroy(list->next);
        free(list);
        return;
    }
}