/*=============================================================================
 |   Assignment:  Pex 1
 |       Author:  Tylar Hanson, Lucas Mireles
 |        Class:  CS483
 +-----------------------------------------------------------------------------
 |   Description:  DESCRIBE THE PROBLEM THAT THIS PROGRAM WAS WRITTEN TO
 |      SOLVE.
 |
 |   Required Features Not Included:  DESCRIBE HERE ANY REQUIREMENTS OF
 |      THE ASSIGNMENT THAT THE PROGRAM DOES NOT ATTEMPT TO SOLVE.
 |
 |   Known Bugs:  IF THE PROGRAM DOES NOT FUNCTION CORRECTLY IN SOME
 |      SITUATIONS, DESCRIBE THE SITUATIONS AND PROBLEMS HERE.
 +-----------------------------------------------------------------------------
 |   Documentation Statement:  Referenced our previously build linked lists in
 |   CS223
 *===========================================================================*/
#include "list.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Test program for CS 483 PEX 1 - Part 1 */

int asdf(void) {
    node* list = NULL;
    char* zero = "zero";
    char* one = "one";
    char* two = "two";
    char* three = "three";

    list = list_insert_tail(list, one);
    list = list_insert_tail(list, two);
    list = list_insert_tail(list, three);
    printf("list_insert_tail and list_print test:\n");
    list_print(list,0);
    /* expected output:
    one
    two
    three
    */

    list = list_insert_head(list, zero);
    printf("\nlist_insert_head and list_print test:\n");
    list_print(list,0);
    /* expected output:
    zero
    one
    two
    three
    */

    list = list_remove(list, "one");
    printf("\nlist_remove test (middle):\n");
    list_print(list,0);
    /* expected output:
    zero
    two
    three
    */

    list = list_remove(list, "three");
    printf("\nlist_remove test (tail):\n");
    list_print(list,0);
    /* expected output:
    zero
    two
    */

    list = list_remove(list, "zero");
    printf("\nlist_remove test (head):\n");
    list_print(list,0);
    /* expected output:
    two
    */

    list = list_insert_head(list, zero);
    list = list_insertn(list, one, 2);
    list = list_insertn(list, three, 4);
    printf("\nlist_insertn test:\n");
    list_print(list,0);
    /* expected output:
    zero
    one
    two
    three
    */

    list = list_removen(list, 1);
    printf("\nlist_removen() test\n");
    int i;
    for(i = 1; i < 4; i++) {
        printf("%d: %s\n", i, list_get(list, i));
    }
    /* expected output:
    1: one
    2: two
    3: three
    */

    printf("\nlist_get test:\n");
    for(i = 1; i < 4; i++) {
        printf("%d: %s\n", i, list_get(list, i));
    }
    /* expected output:
    1: one
    2: two
    3: three
    */

    printf("\nlist_printn() test:\n");
    list_printn(list, 2);
    /* expected output:
    two
    */

    list_destroy(list);
    printf("\n\nlist_destroy() test:\n");
    for(i = 1; i < 4; i++) {
        printf("%d: %s\n", i, list_get(list, i));
    }
    /* expected output:
    1:
    2:
    3:
    */

}