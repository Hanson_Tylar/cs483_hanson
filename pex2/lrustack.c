#include <stdio.h>
#include <stdlib.h>
#include "lrustack.h"

void initialize(lrustack* lrus, int maxsize) {
    lrus->head = malloc(sizeof(node));
    lrus->tail = lrus->head;
    lrus->head->next = NULL;
    lrus->head->prev = NULL;
    lrus->tail->next = NULL;
    lrus->tail->prev = NULL;
    lrus->size = 0;
    lrus->maxsize = maxsize;
}

void push(lrustack* lrus, uint32_t pagenum) {
    if(lrus->size == 0){
        node *temp = malloc(sizeof(node));
        temp->pagenum = pagenum;
        temp->prev = NULL;
        temp->next = NULL;
        lrus->head = temp;
        lrus->tail = temp;
        lrus->size += 1;
    }
    else if(lrus->size < lrus->maxsize){
        node *temp = malloc(sizeof(node));
        temp->pagenum = pagenum;
        temp->next = lrus->head;
        lrus->head->prev = temp;
        lrus->head = temp;
        temp->prev = NULL;
        lrus->size += 1;
    }
    else{
        seek_and_remove(lrus, lrus->tail->pagenum);
        push(lrus, pagenum);
    }
}

int seek_and_remove(lrustack* lrus, uint32_t pagenum) {
    int depth = 1;
    if(lrus->size == 0){
        depth = -1;
    }
    else if(lrus->size == 1 && lrus->head->pagenum == pagenum){
        node *temp = lrus->head;
        lrus->head = NULL;
        lrus->tail = NULL;
        free(temp);
        depth = 1;
        lrus->size -= 1;
    }
    else if(lrus->head->pagenum == pagenum){
        node *temp = lrus->head;
        lrus->head = temp->next;
        lrus->head->prev = NULL;
        free(temp);
        depth = 1;
        lrus->size -= 1;
    }
    else if(lrus->tail->pagenum == pagenum){
        node *temp = lrus->tail;
        lrus->tail = temp->prev;
        lrus->tail->next = NULL;
        free(temp);
        depth = lrus->size;
        lrus->size -= 1;
    }
    else{
        node *temp = lrus->head;
        while(temp->pagenum != pagenum && depth < lrus->size - 1){
            temp = temp->next;
            depth++;
        }
        if(temp->pagenum == pagenum) {
            temp->prev->next = temp->next;
            temp->next->prev = temp->prev;
            free(temp);
            lrus->size -= 1;
        }
        else{
            depth = -1;
        }
    }
    return depth;
}

void print_stack(lrustack* lrus) {
    node* temp = lrus->head;
    int ctr = 1;

    while (temp != NULL) {
        printf("Node %d: %d\n", ctr, temp->pagenum);
        ctr++;
        temp = temp->next;
    }
}

void print_stack_backward(lrustack* lrus) {
    node* temp = lrus->tail;
    int ctr = lrus->size;

    while (temp != NULL) {
        printf("Node %d: %d\n", ctr, temp->pagenum);
        ctr--;
        temp = temp->prev;
    }
}

void test_lru_stack(lrustack* lrus) {
    initialize(lrus, 5);   // create stack of size 5

    if (lrus->size != 0) printf("Failed: size = 0\n");     // size = 0?
    if (lrus->maxsize != 5) printf("Failed: maxsize = 5\n");  // maxsize = 5?

    // test size limiting - size should not grow > 5 nodes
    push(lrus, 1);
    push(lrus, 2);
    push(lrus, 3);
    push(lrus, 4);
    push(lrus, 5);
    push(lrus, 6);
    if (lrus->size != 5) printf("Failed: Size != 5, size = %d Check #1\n", lrus->size);

    // stack should be 6-5-4-3-2
    printf("Stack (forward) should be: 6-5-4-3-2:\n");
    print_stack(lrus);
    printf("\n");
    printf("Stack (backward) should be: 2-3-4-5-6:\n");
    print_stack_backward(lrus);
    printf("\n");

    // test removal of tail
    if ((seek_and_remove(lrus, 2) != 5) || (lrus->size != 4)) {
        printf("Failed: Tail removal 1.\n");
    }

    // stack should be 6-5-4-3
    printf("Stack (forward) should be: 6-5-4-3:\n");
    print_stack(lrus);
    printf("\n");
    printf("Stack (backward) should be: 3-4-5-6:\n");
    print_stack_backward(lrus);
    printf("\n");

    // test removal of middle
    if ((seek_and_remove(lrus, 5) != 2)  || (lrus->size != 3)) {
        printf("Failed: Middle Removal 1.\n");
    }
    if ((seek_and_remove(lrus, 4) != 2)  || (lrus->size != 2)) {
        printf("Failed: Middle Removal 2, Size = %d.\n", lrus->size);
    }

    // stack should be 6-3
    printf("Stack (forward) should be: 6-3:\n");
    print_stack(lrus);
    printf("\n");
    printf("Stack (backward) should be: 3-6:\n");
    print_stack_backward(lrus);
    printf("\n");

    // test removal of head with > 1 item
    if ((seek_and_remove(lrus, 6) != 1)  || (lrus->size != 1)) {
        printf("Failed: Head removal > 1 item in stack.\n");
    }

    // stack should be 3
    printf("Stack (forward) should be: 3:\n");
    print_stack(lrus);
    printf("\n");
    printf("Stack (backward) should be: 3:\n");
    print_stack_backward(lrus);
    printf("\n");
    // test removal of head with 1 item
    if ((seek_and_remove(lrus, 3) != 1) || (lrus->size != 0)) {
        printf("Failed: Head removal with 1 item in stack.\n");
    }

    // stack should be empty
    printf("Stack (forward) should be: [empty]:\n");
    print_stack(lrus);
    printf("\n");
    printf("Stack (backward) should be: [empty]:\n");
    print_stack_backward(lrus);
    printf("\n");

    // test size limiting again
    push(lrus, 2);
    push(lrus, 3);
    push(lrus, 4);

    // test removal of tail
    if ((seek_and_remove(lrus, 2) != 3) || (lrus->size != 2)) {
        printf("Failed: Tail Removal 2.\n");
    }

    push(lrus, 5);
    push(lrus, 6);
    push(lrus, 7);
    push(lrus, 8);

    if (lrus->size != 5) printf("Failed: Size != 5 Check #2\n");

    // Stack should be 8-7-6-5-4
    printf("Stack (forward) should be: 8-7-6-5-4:\n");
    print_stack(lrus);
    printf("\n");
    printf("Stack (backward) should be: 4-5-6-7-8:\n");
    print_stack_backward(lrus);
    printf("\n");

    // search for non-existent
    if ((seek_and_remove(lrus, 2) != -1) || (lrus->size != 5)) {
        printf("Failed: Non-existent seek and remove.\n");
    }

    // search/remove tail
    if ((seek_and_remove(lrus, 4) != 5) || (lrus->size != 4)) {
        printf("Failed: Tail Removal 3.\n");
    }

    // search/remove head
    if ((seek_and_remove(lrus, 8) != 1) || (lrus->size != 3)) {
        printf("Failed: Head Removal Check 3.\n");
    }

    // Stack should be 7-6-5
    printf("Stack (forward) should be: 7-6-5:\n");
    print_stack(lrus);
    printf("\n");
    printf("Stack (backward) should be: 5-6-7:\n");
    print_stack_backward(lrus);
    printf("\n");

    printf("test_lru() complete.\n");
}