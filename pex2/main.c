#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include "byutr.h"
#include "lrustack.h"

/*=============================================================================
 |   Assignment:  PEX2
 |       Author:  C2C Tylar Hanson, C2C Lucas Mireles
 |        Class:  CS483
 +-----------------------------------------------------------------------------
 |   Description:  DESCRIBE THE PROBLEM THAT THIS PROGRAM WAS WRITTEN TO
 |      SOLVE.
 |
 |   Required Features Not Included:  DESCRIBE HERE ANY REQUIREMENTS OF
 |      THE ASSIGNMENT THAT THE PROGRAM DOES NOT ATTEMPT TO SOLVE.
 |
 |   Known Bugs:  IF THE PROGRAM DOES NOT FUNCTION CORRECTLY IN SOME
 |      SITUATIONS, DESCRIBE THE SITUATIONS AND PROBLEMS HERE.
 +-----------------------------------------------------------------------------
 |   Documentation Statement:  None
 *===========================================================================*/

int main(int argc, char **argv) {
    FILE *ifp;
    uint64_t num_accesses = 0;
    p2AddrTr trace_record;
    int depth;
    lrustack lrus;


//    test_lru_stack(&lrus);
//    return 0;
    // If the number of arguments is wrong then quit
    if (argc != 2 && argc != 3) {
        fprintf(stderr, "usage: %s input_byutr_file [frame_size]\n", argv[0]);
        fprintf(stderr,
                "\nwhere frame_size is a digit corresponding to the following menu:\n\t1: 512 bytes\n\t2: 1KB\n\t3: 2KB\n\t4: 4KB\n");
        exit(1);
    }

    // If the file cannot be opened, then quit
    if ((ifp = fopen(argv[1], "rb")) == NULL) {
        fprintf(stderr, "cannot open %s for reading\n", argv[1]);
        exit(1);
    }

    int menu_option = 0;
    if (argc == 3) {
        menu_option = atoi(argv[2]);
    }
    while (menu_option < 1 || menu_option > 4) {
        char input[128];
        fprintf(stderr, "Select a frame size:\n\t1: 512 bytes\n\t2: 1KB\n\t3: 2KB\n\t4: 4KB\n");
        fgets(input, 128, stdin);
        menu_option = atoi(input);
    }
    int OFFSET_BITS = 0;
    int MAX_FRAMES = 0;//THIS WILL BE USED AS THE SIZE OF YOUR ARRAY
    switch (menu_option) {
        case 1:
            OFFSET_BITS = 9;
            MAX_FRAMES = 8192;
            break;
        case 2:
            OFFSET_BITS = 10;
            MAX_FRAMES = 4096;
            break;
        case 3:
            OFFSET_BITS = 11;
            MAX_FRAMES = 2048;
            break;
        case 4:
            OFFSET_BITS = 12;
            MAX_FRAMES = 1024;
            break;
    }

    initialize(&lrus, MAX_FRAMES);
    int pf_array[MAX_FRAMES];
    for(int i = 0; i <MAX_FRAMES; i++){
        pf_array[i] = 0;
    }
    while (!feof(ifp))
    //while(!feof(ifp) && i < 100)  //you may want to use this to debug
    {
        //read next trace record
        fread(&trace_record, sizeof(p2AddrTr), 1, ifp);
        //to get the page number, we shift the offset bits off of the address

        uint32_t page_num = trace_record.addr >> OFFSET_BITS;   // THIS IS THE PAGE NUMBER THAT WAS REQUESTED!!!!!

        // this next line prints the page number that was referenced.
        // Might be useful when debugging.
        // The template will not compile with this line uncommented...you may will need to comment it
        // out prior to submission
        //printf("page#: %d maxframes:%d\n", page_num, MAX_FRAMES);

        num_accesses++;

        // more code possibly useful for debugging... gives an indication of progress being made
        // when running the full trace
        //if((num_accesses % 100000) == 0){
        //	printf("%d samples read\r", num_accesses);
        //  fflush(stdout);
        //}

        //TODO: process each page reference
        depth = seek_and_remove(&lrus, page_num);
        push(&lrus, page_num);
        if (depth == -1) {
            for (int i = 1; i<MAX_FRAMES; i++) {
                pf_array[i]++;
            }
        } else {
            for (int i = depth-1; i>0; i--) {
                pf_array[i]++;
            }
        }
    }

    //TODO: find the number of page faults for each number of allocated frames

    fclose(ifp);
//    print_stack(&lrus);
    printf("Total Accesses: %d\nFrames\tMiss Rate\n", num_accesses);
    for(int i=1; i<MAX_FRAMES; i++){
        printf("%d\t\t%7.6f\n", i, (float)pf_array[i]/(float)num_accesses);
    }
    return (0);
}