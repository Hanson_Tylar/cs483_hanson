/* Author: C2C Tylar Hanson and C2C Lucas Mireles
 * Course: Operating Systems
 * Assignment: Pex1, A Simple Shell
 * Documentation: Many sections of this code were created after referencing lesson handouts on the moodle website.
 * I used stack exchange to look up printf() color codes.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include "list.h"

#define MAX_STR_LEN 128
#define KGRN  "\x1B[32m"
#define KWHT  "\x1B[37m"

void fork_and_execute(char **command){
    // Create a child process and execute the given internal command
    pid_t ret_val;
    int status = 0;
    ret_val = fork();
    if(ret_val < 0){
        printf("\nError with fork: Child process not created.\n");
        return;
    }
    else if(ret_val == 0){
        if(execvp(command[0], command) < 0){
            printf("\nError with execvp: please give a valid command.\n\n");
            return;
        }
    }
    else{
        wait(&status);
    }
    return;
}
void parse(char *input_str, char **parsed_str_array){
    // This section of code breaks up the user input by spaces and
    // stores it in an array of words to be used by execvp later
    char *word;
    int i = 0;
    word = strtok(input_str, " \t");
    while(word != NULL){
        parsed_str_array[i] = strdup(word);
        word = strtok (NULL, " \t");
        i++;
    }
    parsed_str_array[i] = NULL;
}

void main(){
    node *command_history = NULL;
    char current_directory[MAX_STR_LEN];
    char *input_str = malloc(MAX_STR_LEN);
    char *parsed_str_array[16] = {""};
    int cmd_recalled = 0;
    char *recall_cmd;
    int recall_number;

    while(1) {

        // print current directory and get user input if not recalling a command
        if(cmd_recalled == 0){
            getcwd(current_directory, sizeof(current_directory));
            printf("%s:%s$ %s", KGRN, current_directory, KWHT);
            fgets(input_str, MAX_STR_LEN, stdin);
            if(strlen(input_str)>1){
                input_str[strlen(input_str)-1] = '\0';
            }
        }
        cmd_recalled = 0;

        // Store the command history in a linked list
        command_history = list_remove(command_history, input_str);
        command_history = list_insert_head(command_history, input_str);


        parse(input_str, parsed_str_array);


        if(!strcmp(parsed_str_array[0], "exit")){
            while(1){exit(1);}
        }
        else if(!strcmp(parsed_str_array[0], "history")){
            list_print(command_history, 1);
        }
        else if(!strcmp(parsed_str_array[0], "recall")){
            if(parsed_str_array[1] == NULL){
                printf("Error with Recall: expected two arguments.\n");
                cmd_recalled = 0;
            }
            else if( list_get(command_history, atoi(parsed_str_array[1])+1) == NULL){
                printf("Error with Recall: Command does not exist.\n");
                cmd_recalled = 0;
            }
            else{
                cmd_recalled = 1;
                recall_number = atoi(parsed_str_array[1])+1;
                recall_cmd = list_get(command_history, recall_number);
                strncpy(input_str, recall_cmd, MAX_STR_LEN);
            }

        }
        else if(!strcmp(parsed_str_array[0], "cd")){
            int ret_val;
            if(!strcmp(parsed_str_array[1], "~")){
                ret_val = chdir(getenv("HOME"));
            }
            else {
                ret_val = chdir(parsed_str_array[1]);
            }
            if(ret_val != 0){
                printf("Error with chdir()");
                exit;
            }
        }
        else{
            fork_and_execute(parsed_str_array);
        }

    }
}